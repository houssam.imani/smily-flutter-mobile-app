# Smily

Smily est une application mobile qui a pour but d'aider ses utilisateurs à évaluer leurs états émotionnels et à écrire ce qu'ils ressentent dans un journal de bord

## Fonctionnalités

Authentification : Inscription, connexion, Mise en session de l'utilisateur
Bilan : Enregistrement d'un bilan avec l'humeur, les textes et les activités
Notification : Automatisation de notification simple à 20h00 tous les jours pour le moment.
Une structure front quasi complète.

Fonctionnalité en cours :
Mise à jour du profil depuis le formulaire prévu a cet effet! 

## Mise en garde

La gestion des notifications ne fonctionne que sur Android.
