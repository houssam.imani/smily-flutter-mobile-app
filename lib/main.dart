import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:smily/config/palette.dart';
import 'package:smily/layout/all_layout.dart';
import 'package:smily/model/activity_provider.dart';
import 'package:smily/model/review_provider.dart';
import 'package:smily/model/text_note_provider.dart';
import 'package:smily/model/user_provider.dart';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:smily/components/notification/notification.dart';

void main() {
  AwesomeNotifications().initialize(null, [
    NotificationChannel(
      channelKey: 'scheduled_channel',
      channelName: 'Scheduled Notification',
      channelDescription: '',
      playSound: true,
      enableVibration: true,
      defaultColor: Palette.smilyPurple,
      locked: true,
      importance: NotificationImportance.High,
    )
  ]);
  runApp(const Smily());
}

class Smily extends StatefulWidget {
  const Smily({Key? key}) : super(key: key);

  @override
  State<Smily> createState() => _SmilyState();
}

class _SmilyState extends State<Smily> {
  final UserProvider userProvider = UserProvider();
  final ReviewProvider reviewProvider = ReviewProvider();
  final ActivityProvider activityProvider = ActivityProvider();
  final TextNoteProvider textNoteProvider = TextNoteProvider();

  @override
  Widget build(BuildContext context) {
    /*cancelScheduledNotifications();*/
    createReminderNotification('Smily - Remplissez votre bilan !',
        'Comment vous sentez-vous aujourd\'hui ?');

    return MultiProvider(
      providers: [
        ChangeNotifierProvider<UserProvider>(
          create: (_) => userProvider,
        ),
        ChangeNotifierProvider<ReviewProvider>(
          create: (_) => reviewProvider,
        ),
        ChangeNotifierProvider<ActivityProvider>(
          create: (_) => activityProvider,
        ),
        ChangeNotifierProvider<TextNoteProvider>(
          create: (_) => textNoteProvider,
        ),
      ],
      child: MaterialApp(
        title: 'Smily',
        theme: ThemeData(
            primarySwatch: Palette.smilyPurple,
            textTheme: const TextTheme(
              bodyText2: TextStyle(
                fontFamily: "Roboto",
                fontSize: 18,
                color: Palette.smilyGrey,
              ),
              button: TextStyle(
                fontFamily: "Roboto",
                fontSize: 18,
              ),
            )),
        debugShowCheckedModeBanner: false,
        initialRoute: '/',
        routes: {
          '/': (context) => const Accueil(),
          '/mission': (context) => const Mission(),
          '/inscription': (context) => const Inscription(),
          '/connexion': (context) => const Connexion(),
          '/mdp-oublie': (context) => const MdpOublie(),
          '/dashboard': (context) => const Dashboard(),
          '/notification': (context) => const NotificationSetup(),
          '/bilan': (context) => const Bilan(),
          '/bilan-icons': (context) => const BilanIcons(),
          '/profile': (context) => const Profile(),
          '/journal': (context) => const Journal(),
          '/parametres': (context) => const Parametres(),
        },
      ),
    );
  }
}
