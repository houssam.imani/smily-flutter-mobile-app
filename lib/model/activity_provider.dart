import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;

import 'dart:collection'; // nouveaux type de listes comme UnmodifiableListView
import 'dart:convert'; // pour decoder la réponse http

import 'activity_model.dart';

// Commandes utiles :
// Lancer le serveur node (attendre le message "connexion ok !")
// backend> npm start

class ActivityProvider with ChangeNotifier {
  final String host = 'http://localhost:3000';
  List<Activity> _activities = [];

  // Getter pour l'accès en lecture de l'ensemble des activités
  // Pas de modificiation possible grâce au type UnmodifiableListView
  UnmodifiableListView<Activity> get activities =>
      UnmodifiableListView(_activities);

  // Récupérer les données dans la base de données
  Future<http.Response?> fetchData(String reviewId) async {
    try {
      http.Response response =
          await http.get(Uri.parse('$host/api/activities/review/' + reviewId));
      if (response.statusCode == 200) {
        _activities = (json.decode(response.body) as List)
            .map((activityJson) => Activity.fromJson(activityJson))
            .toList();
        notifyListeners();
      }
      return response;
    } catch (e) {
      rethrow;
    }
  }

  // Ajouter une activité dans la base de données
  Future<http.Response?> addActivity(Activity newActivity) async {
    try {
      http.Response response = await http.post(
        Uri.parse('$host/api/activities'),
        body: json.encode(newActivity.toJson()),
        headers: {'Content-type': 'application/json'},
      );
      if (response.statusCode == 200) {
        _activities.add(
          Activity.fromJson(
            json.decode(response.body),
          ),
        );
        notifyListeners();
      }
      return response;
    } catch (e) {
      rethrow;
    }
  }
}
