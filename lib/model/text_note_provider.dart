import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;

import 'dart:collection'; // nouveaux type de listes comme UnmodifiableListView
import 'dart:convert'; // pour decoder la réponse http

import 'text_note_model.dart';

// Commandes utiles :
// Lancer le serveur node (attendre le message "connexion ok !")
// backend> npm start

class TextNoteProvider with ChangeNotifier {
  final String host = 'http://localhost:3000';
  List<TextNote> _textNotes = [];

  // Getter pour l'accès en lecture de l'ensemble des activités
  // Pas de modificiation possible grâce au type UnmodifiableListView
  UnmodifiableListView<TextNote> get textNotes =>
      UnmodifiableListView(_textNotes);

  // Récupérer les données dans la base de données
  Future<http.Response?> fetchData(String reviewId) async {
    try {
      http.Response response =
          await http.get(Uri.parse('$host/api/textnotes/review/' + reviewId));
      if (response.statusCode == 200) {
        _textNotes = (json.decode(response.body) as List)
            .map((textNoteJson) => TextNote.fromJson(textNoteJson))
            .toList();
        notifyListeners();
      }
      return response;
    } catch (e) {
      rethrow;
    }
  }

  // Ajouter une note écrite dans la base de données
  Future<http.Response?> addTextNote(TextNote newTextNote) async {
    try {
      http.Response response = await http.post(
        Uri.parse('$host/api/textnotes'),
        body: json.encode(newTextNote.toJson()),
        headers: {'Content-type': 'application/json'},
      );
      if (response.statusCode == 200) {
        _textNotes.add(
          TextNote.fromJson(
            json.decode(response.body),
          ),
        );
        notifyListeners();
      }
      return response;
    } catch (e) {
      rethrow;
    }
  }
}
