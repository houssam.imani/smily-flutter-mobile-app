import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;
import 'package:smily/components/smily.dart';
import 'dart:convert'; // pour decoder la réponse http

import 'user_model.dart';

// Commandes utiles :
// Lancer le serveur node (attendre le message "connexion ok !")
// backend> npm start

class UserProvider with ChangeNotifier {
  final String host = 'http://localhost:3000';

  User user = User(email: '', password: '', username: '');

  // Récupérer les données dans la base de données
  void fetchData() async {
    try {
      http.Response response =
          await http.get(Uri.parse('$host/api/users/${Session.userId}'));
      if (response.statusCode == 200) {
        user = User.fromJson(response.body);
        notifyListeners();
      }
    } catch (e) {
      rethrow;
    }
  }

  // Ajouter un profile dans la base de données
  Future<http.Response?> addUser(User newUser) async {
    try {
      http.Response response = await http.post(
        Uri.parse('$host/api/users'),
        body: json.encode(newUser.toJson()),
        headers: {'Content-type': 'application/json'},
      );
      if (response.statusCode == 200) {
        notifyListeners();
      }
      return response;
    } catch (e) {
      rethrow;
    }
  }

  Future<http.Response?> logUser(User user) async {
    try {
      http.Response response = await http.post(
        Uri.parse('$host/api/users/login'),
        body: json.encode(user.toJson()),
        headers: {'Content-type': 'application/json'},
      );
      return response;
    } catch (e) {
      rethrow;
    }
  }

  // Modifier un profile dans la base de données

  Future<http.Response?> updateUser(User updateUser) async {
    try {
      http.Response response = await http.post(
        Uri.parse('$host/api/users/profile/update/${Session.userId}'),
        body: json.encode(updateUser.toJson()),
        headers: {'Content-type': 'application/json'},
      );
      if (response.statusCode == 200) {
        user = User.fromJson(response.body);
        notifyListeners();
      }
      return response;
    } catch (e) {
      rethrow;
    }
  }
}
