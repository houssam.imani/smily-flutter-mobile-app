import 'package:flutter/material.dart';

import 'package:http/http.dart' as http;

import 'dart:collection'; // nouveaux type de listes comme UnmodifiableListView
import 'dart:convert'; // pour decoder la réponse http

import 'review_model.dart';

// Commandes utiles :
// Lancer le serveur node (attendre le message "connexion ok !")
// backend> npm start

class ReviewProvider with ChangeNotifier {
  final String host = 'http://localhost:3000';
  List<Review> _reviews = [];

  // Getter pour l'accès en lecture de l'ensemble des bilans
  // Pas de modificiation possible grâce au type UnmodifiableListView
  UnmodifiableListView<Review> get reviews => UnmodifiableListView(_reviews);

  // Récupérer les données dans la base de données
  Future<http.Response?> fetchData(String userId) async {
    try {
      http.Response response =
          await http.get(Uri.parse('$host/api/reviews/user/' + userId));
      if (response.statusCode == 200) {
        _reviews = (json.decode(response.body) as List)
            .map((reviewJson) => Review.fromJson(reviewJson))
            .toList();
        notifyListeners();
      }
      return response;
    } catch (e) {
      rethrow;
    }
  }

  // Ajouter un bilan dans la base de données
  Future<http.Response?> addReview(Review newReview) async {
    try {
      http.Response response = await http.post(
        Uri.parse('$host/api/reviews'),
        body: json.encode(newReview.toJson()),
        headers: {'Content-type': 'application/json'},
      );
      if (response.statusCode == 200) {
        _reviews.add(
          Review.fromJson(
            json.decode(response.body),
          ),
        );
        notifyListeners();
      }
      return response;
    } catch (e) {
      rethrow;
    }
  }
}
