class Review {
  String? id;
  String userId;
  String date;
  String time;
  String mood;

  Review({
    this.id,
    required this.userId,
    required this.date,
    required this.time,
    required this.mood,
  });

  Review.fromJson(Map<String, dynamic> json)
      : id = json['_id'],
        userId = json['userId'],
        date = json['date'],
        time = json['time'],
        mood = json['mood'];

  Map<String, dynamic> toJson() {
    if (id != null) {
      return {
        '_id': id,
        'userId': userId,
        'date': date,
        'time': time,
        'mood': mood,
      };
    } else {
      return {
        'userId': userId,
        'date': date,
        'time': time,
        'mood': mood,
      };
    }
  }

  String showReview() {
    return "$date\n$time\n$mood\n\n";
  }
}
