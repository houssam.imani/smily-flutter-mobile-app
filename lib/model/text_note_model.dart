class TextNote {
  String? id;
  String reviewId;
  String note;

  TextNote({
    this.id,
    required this.reviewId,
    required this.note,
  });

  TextNote.fromJson(Map<String, dynamic> json)
      : id = json['_id'],
        reviewId = json['reviewId'],
        note = json['note'];

  Map<String, dynamic> toJson() {
    if (id != null) {
      return {
        '_id': id,
        'reviewId': reviewId,
        'note': note,
      };
    } else {
      return {
        'reviewId': reviewId,
        'note': note,
      };
    }
  }

  String showTextNote() {
    return "$note\n\n";
  }
}
