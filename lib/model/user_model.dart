import 'dart:convert';

class User {
  String? id;
  String email;
  String? username;
  String password;
  String? photo;
  DateTime? birthday;
  String? firstname;
  String? lastname;
  int? sex;

  User({
    this.id,
    required this.email,
    this.username,
    required this.password,
    this.photo,
    this.birthday,
    this.firstname,
    this.lastname,
    this.sex,
  });

  User.fromJson(String json)
      : id = jsonDecode(json)['_id'],
        email = jsonDecode(json)['email'],
        username = jsonDecode(json)['username'],
        password = jsonDecode(json)['password'],
        photo = jsonDecode(json)['photo'],
        birthday = jsonDecode(json)['birthday'],
        firstname = jsonDecode(json)['firstname'],
        lastname = jsonDecode(json)['lastname'],
        sex = jsonDecode(json)['sex'];

  Map<String, dynamic> toJson() {
    if (id != null) {
      return {
        '_id': id,
        'email': email,
        'username': username,
        'password': password,
        'photo': photo,
        'birthday': birthday,
        'firstname': firstname,
        'lastname': lastname,
        'sex': sex,
      };
    } else {
      return {
        'email': email,
        'username': username,
        'password': password,
        'photo': photo,
        'birthday': birthday,
        'firstname': firstname,
        'lastname': lastname,
        'sex': sex,
      };
    }
  }

  String showUser() {
    return "$email\n$password\n\n";
  }
}
