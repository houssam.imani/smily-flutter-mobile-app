class Activity {
  String? id;
  String reviewId;
  String activity;

  Activity({
    this.id,
    required this.reviewId,
    required this.activity,
  });

  Activity.fromJson(Map<String, dynamic> json)
      : id = json['_id'],
        reviewId = json['reviewId'],
        activity = json['activity'];

  Map<String, dynamic> toJson() {
    if (id != null) {
      return {
        '_id': id,
        'reviewId': reviewId,
        'activity': activity,
      };
    } else {
      return {
        'reviewId': reviewId,
        'activity': activity,
      };
    }
  }

  String showActivity() {
    return "$activity\n\n";
  }
}
