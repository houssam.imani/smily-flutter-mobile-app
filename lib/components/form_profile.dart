import 'package:flutter/material.dart';
import 'package:smily/components/smily.dart';

class SmilyProfileForm extends StatelessWidget {
  final List<Widget> children;
  final GlobalKey<FormState>? formKey;

  const SmilyProfileForm({Key? key, required this.children, this.formKey})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.only(top: 10, bottom: 10),
        child: Form(
          key: formKey,
          child: Column(
              textDirection: TextDirection.ltr,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: children),
        ));
  }
}

class SmilyProfileTextFile extends StatelessWidget {
  final String label;

  const SmilyProfileTextFile({Key? key, required this.label}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 6, bottom: 6),
      child: TextField(
          decoration: InputDecoration(
            border: InputBorder.none,
            filled: true,
            fillColor: Palette.smilySuperLightGrey,
            labelText: label,
          ),
          style: const TextStyle(
            color: Palette.smilyDarkGrey,
            height: 1.5,
          )),
    );
  }
}

class SmilyProfileLabel extends StatelessWidget {
  final String label;

  const SmilyProfileLabel({Key? key, required this.label}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 7, bottom: 7),
      child: Text(
        label,
        style: const TextStyle(
          fontSize: 16,
          fontWeight: FontWeight.w500,
          color: Palette.smilyGrey,
        ),
      ),
    );
  }
}
