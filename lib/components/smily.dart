import 'package:flutter/material.dart';
import 'package:smily/config/palette.dart';
import 'package:smily/model/review_model.dart';
export 'package:smily/config/palette.dart';

class Session {
  static String? userId;
  static String? token;

  static Review? review;
  static List<String>? textNotes;

  static void redirectIfNotConnected(BuildContext context) {
    if (token == null) {
      Future.delayed(Duration.zero, () {
        Navigator.popAndPushNamed(context, '/connexion');
      });
    }
  }

  static void flush() {
    userId = null;
    token = null;
    reviewFlush();
  }

  static void reviewFlush() {
    review = null;
    textNotes = null;
  }
}

class SmilyScaffoldScroll extends StatelessWidget {
  final List<Widget> children;
  final Color backgroundColor;
  final EdgeInsetsGeometry? margin;
  final bool navBar;

  const SmilyScaffoldScroll({
    Key? key,
    required this.children,
    this.backgroundColor = Colors.white,
    this.margin,
    this.navBar = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      body: SingleChildScrollView(
        child: Container(
          margin: margin,
          child: Column(
            children: children,
          ),
        ),
      ),
      bottomNavigationBar: navBar ? const SmilyNavBar() : null,
      floatingActionButtonLocation:
          navBar ? FloatingActionButtonLocation.centerDocked : null,
      floatingActionButton: navBar
          ? FloatingActionButton(
              backgroundColor: Palette.smilyPurple,
              child: const Icon(
                Icons.add,
                color: Colors.white,
              ),
              onPressed: () => {Navigator.pushNamed(context, '/bilan')},
            )
          : null,
    );
  }
}

class SmilyNavBar extends StatelessWidget {
  const SmilyNavBar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      color: Palette.smilyPurple,
      shape: const CircularNotchedRectangle(),
      notchMargin: 6,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Container(
            height: 60,
            margin: const EdgeInsets.all(5),
            child: Column(
              children: [
                IconButton(
                  icon: const Icon(
                    Icons.settings,
                    color: Colors.white,
                  ),
                  onPressed: () =>
                      {Navigator.pushNamed(context, '/parametres')},
                ),
                const Text(
                  "Paramètres",
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 60,
            margin: const EdgeInsets.fromLTRB(5, 5, 30, 5),
            child: Column(
              children: [
                IconButton(
                  icon: const Icon(
                    Icons.person,
                    color: Colors.white,
                  ),
                  onPressed: () => {Navigator.pushNamed(context, '/profile')},
                ),
                const Text(
                  "Profile",
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 60,
            margin: const EdgeInsets.fromLTRB(30, 5, 5, 5),
            child: Column(
              children: [
                IconButton(
                  icon: const Icon(
                    Icons.stacked_bar_chart,
                    color: Colors.white,
                  ),
                  onPressed: () => {Navigator.pushNamed(context, '/dashboard')},
                ),
                const Text(
                  "Statistiques",
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
          Container(
            height: 60,
            margin: const EdgeInsets.all(5),
            child: Column(
              children: [
                IconButton(
                  icon: const Icon(
                    Icons.list,
                    color: Colors.white,
                  ),
                  onPressed: () => {Navigator.pushNamed(context, '/journal')},
                ),
                const Text(
                  "Journal",
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.white,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
