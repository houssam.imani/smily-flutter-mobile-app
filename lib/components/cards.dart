import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:smily/config/palette.dart';

class SmilyCard extends StatelessWidget {
  final Widget? child;
  final EdgeInsetsGeometry? margin;
  final EdgeInsetsGeometry? padding;
  final double? elevation;
  final Color? color;
  final double borderRadius;

  const SmilyCard({
    Key? key,
    this.child,
    this.margin = const EdgeInsets.only(top: 25),
    this.padding = const EdgeInsets.symmetric(horizontal: 20, vertical: 28),
    this.elevation = 4,
    this.color,
    this.borderRadius = 15,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: margin,
      elevation: elevation,
      color: color,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(borderRadius),
      ),
      child: Container(
        margin: padding,
        child: child,
      ),
    );
  }
}

class SmilySectionCard extends StatelessWidget {
  final Widget? child;
  final String title;

  const SmilySectionCard({
    Key? key,
    this.child,
    required this.title,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SmilyCard(
      padding: null,
      child: Column(
        children: [
          Container(
            width: double.infinity,
            padding: const EdgeInsets.fromLTRB(35, 15, 35, 10),
            decoration: const BoxDecoration(
              border: Border(
                bottom: BorderSide(color: Palette.smilyLightGrey),
              ),
            ),
            child: Text(
              title,
              style: const TextStyle(
                color: Palette.smilyDarkGrey,
                fontSize: 40,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 45, vertical: 30),
            child: child,
          )
        ],
      ),
    );
  }
}

class SmilyFlatCard extends StatelessWidget {
  final Widget? child;
  final Color? color;

  const SmilyFlatCard({
    Key? key,
    this.child,
    this.color = Palette.smilyWhiteGrey,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SmilyCard(
      color: color,
      elevation: 0,
      child: child,
    );
  }
}

class SmilyBadge extends StatelessWidget {
  final String? icon;
  final String label;

  const SmilyBadge({
    Key? key,
    this.icon,
    required this.label,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SmilyCard(
      color: Palette.smilyPurple,
      padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      margin: null,
      elevation: 0,
      borderRadius: 8,
      child: Row(
        children: [
          Container(
            margin: icon != null ? const EdgeInsets.only(right: 15) : null,
            child: icon != null
                ? SvgPicture.asset(
                    icon!,
                    width: 15,
                    color: Colors.white,
                  )
                : null,
          ),
          Text(
            label,
            style: const TextStyle(
              color: Colors.white,
              fontWeight: FontWeight.bold,
            ),
          ),
        ],
      ),
    );
  }
}

enum ActivityType {
  malade,
  travail,
  salleSport,
  cinema,
  hopital,
  maison,
  meditation,
  sport,
  marche,
  balade,
  plage
}

String getActivityTypeIcon(ActivityType activityType) {
  switch (activityType) {
    case ActivityType.malade:
      return 'img/icons/activity/bed-pulse-solid.svg';
    case ActivityType.travail:
      return 'img/icons/activity/briefcase-solid.svg';
    case ActivityType.salleSport:
      return 'img/icons/activity/dumbbell-solid.svg';
    case ActivityType.cinema:
      return 'img/icons/activity/film-solid.svg';
    case ActivityType.hopital:
      return 'img/icons/activity/hospital-solid.svg';
    case ActivityType.maison:
      return 'img/icons/activity/house-solid.svg';
    case ActivityType.meditation:
      return 'img/icons/activity/person-praying-solid.svg';
    case ActivityType.sport:
      return 'img/icons/activity/person-running-solid.svg';
    case ActivityType.marche:
      return 'img/icons/activity/person-walking-solid.svg';
    case ActivityType.balade:
      return 'img/icons/activity/tree-solid.svg';
    case ActivityType.plage:
      return 'img/icons/activity/umbrella-beach-solid.svg';
  }
}

String getActivityTypeLabel(ActivityType activityType) {
  switch (activityType) {
    case ActivityType.malade:
      return 'Malade';
    case ActivityType.travail:
      return 'Travail';
    case ActivityType.salleSport:
      return 'Salle de sport';
    case ActivityType.cinema:
      return 'Cinéma';
    case ActivityType.hopital:
      return 'Hôpital';
    case ActivityType.maison:
      return 'Maison';
    case ActivityType.meditation:
      return 'Méditation';
    case ActivityType.sport:
      return 'Sport';
    case ActivityType.marche:
      return 'Marche';
    case ActivityType.balade:
      return 'Balade';
    case ActivityType.plage:
      return 'Plage';
  }
}

class SmilyActivityType extends StatelessWidget {
  final ActivityType type;

  const SmilyActivityType({Key? key, required this.type}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SmilyCard(
      padding: null,
      margin: null,
      elevation: 0,
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: [
          Container(
            margin: const EdgeInsets.only(right: 8),
            child: SvgPicture.asset(
              getActivityTypeIcon(type),
              width: type == ActivityType.marche ? 11 : 14,
              color: Palette.smilyPurple,
            ),
          ),
          Text(
            getActivityTypeLabel(type),
            style: const TextStyle(fontSize: 16),
          )
        ],
      ),
    );
  }
}

class SmilyActivityIcon extends StatefulWidget {
  final ActivityType type;
  final Function(String) sendActivity;
  const SmilyActivityIcon({
    Key? key,
    required this.sendActivity,
    required this.type,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SmilyActivityIconState();
}

class _SmilyActivityIconState extends State<SmilyActivityIcon> {
  late bool _selected;

  @override
  void initState() {
    _selected = false;
    super.initState();
  }

  void select() {
    setState(() {
      _selected = !_selected;
      widget.sendActivity(describeEnum(widget.type));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.only(bottom: 10),
          child: Text(
            getActivityTypeLabel(widget.type),
            style: const TextStyle(fontSize: 16),
          ),
        ),
        InkWell(
          onTap: select,
          child: Container(
            padding: const EdgeInsets.all(12),
            height: 62,
            width: 62,
            decoration: ShapeDecoration(
              color: _selected ? Palette.smilyPurple.withOpacity(0.3) : null,
              shape: const CircleBorder(
                side: BorderSide(
                  width: 4,
                  color: Palette.smilyPurple,
                ),
              ),
            ),
            child: SvgPicture.asset(
              getActivityTypeIcon(widget.type),
              fit: BoxFit.scaleDown,
              color: Palette.smilyPurple,
            ),
          ),
        ),
      ],
    );
  }
}

class SmilyAlertDialog extends StatelessWidget {
  final List<Widget> children;
  const SmilyAlertDialog({Key? key, required this.children}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: const EdgeInsets.fromLTRB(24, 4, 24, 24),
      content: SizedBox(
        width: MediaQuery.of(context).size.width - 100,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: children,
        ),
      ),
    );
  }
}
