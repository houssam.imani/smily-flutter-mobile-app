import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:smily/config/palette.dart';

enum ButtonType { next, send, save, commit }

class SmilyButton extends StatelessWidget {
  final String text;
  final ButtonType type;
  final void Function()? action;
  final bool colorReverse;
  final double width;
  final EdgeInsetsGeometry? margin;
  final bool active;

  const SmilyButton(
    this.text, {
    Key? key,
    this.type = ButtonType.commit,
    this.action,
    this.colorReverse = false,
    this.width = double.infinity,
    this.margin,
    this.active = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      child: ElevatedButton(
        style: ButtonStyle(
          fixedSize: MaterialStateProperty.all<Size>(Size(width, 70)),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(40.0),
            ),
          ),
          backgroundColor: MaterialStateProperty.all<Color>(
            active
                ? colorReverse
                    ? Colors.white
                    : Palette.smilyPurple
                : Palette.smilyLightGrey,
          ),
        ),
        onPressed: active ? action : null,
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 15),
          child: Row(
            children: [
              Expanded(
                child: Text(
                  text,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: colorReverse ? Palette.smilyPurple : Colors.white,
                    fontSize: 22,
                  ),
                ),
              ),
              SvgPicture.asset(
                getIcon(type),
                width: type == ButtonType.next ? 14 : 24,
                color: colorReverse ? Palette.smilyPurple : Colors.white,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

String getIcon(ButtonType type) {
  if (type == ButtonType.next) {
    return 'img/icons/angle-right-solid.svg';
  } else if (type == ButtonType.save) {
    return 'img/icons/floppy-disk-solid.svg';
  } else if (type == ButtonType.send) {
    return 'img/icons/paper-plane-solid.svg';
  } else {
    return 'img/icons/check-solid.svg';
  }
}

class SmilyTextButton extends StatelessWidget {
  final String text;
  final EdgeInsetsGeometry? margin;
  final AlignmentGeometry alignment;
  final void Function()? action;
  final Color color;

  const SmilyTextButton(this.text,
      {Key? key,
      this.margin,
      this.action,
      this.color = Palette.smilyPurple,
      this.alignment = Alignment.center})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: alignment,
      margin: margin,
      child: TextButton(
        onPressed: action,
        child: Text(
          text,
          style: TextStyle(
            color: color,
          ),
        ),
      ),
    );
  }
}

class SmilyBottomButton extends StatelessWidget {
  final String text;
  final void Function()? action;
  final ButtonType type;
  final bool active;

  const SmilyBottomButton(
    this.text, {
    Key? key,
    this.action,
    this.type = ButtonType.next,
    this.active = true,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Positioned(
      child: ElevatedButton(
        style: ButtonStyle(
          fixedSize:
              MaterialStateProperty.all<Size>(const Size(double.infinity, 70)),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(0),
            ),
          ),
          backgroundColor: MaterialStateProperty.all<Color>(
            active ? Palette.smilyPurple : Palette.smilyLightGrey,
          ),
        ),
        onPressed: active ? action : null,
        child: Container(
          margin: const EdgeInsets.symmetric(horizontal: 15),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                text,
                textAlign: TextAlign.center,
                style: const TextStyle(
                  color: Colors.white,
                  fontSize: 22,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(left: 15),
                child: SvgPicture.asset(
                  getIcon(type),
                  width: type == ButtonType.next ? 14 : 24,
                  color: Colors.white,
                ),
              ),
            ],
          ),
        ),
      ),
      bottom: 0,
      left: 0,
      right: 0,
    );
  }
}
