import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:smily/components/cards.dart';
export 'package:smily/config/palette.dart';

class MoodChoice extends StatefulWidget {
  final Function(String) sendMood;
  const MoodChoice({Key? key, required this.sendMood}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MoodChoiceState();
}

class _MoodChoiceState extends State<MoodChoice> {
  late SmileyType? _moodSelect;

  @override
  void initState() {
    _moodSelect = null;
    super.initState();
  }

  void select(SmileyType selection) {
    setState(() {
      _moodSelect = selection;
      widget.sendMood(describeEnum(selection));
    });
  }

  @override
  Widget build(BuildContext context) {
    return SmilyCard(
      margin: const EdgeInsets.symmetric(vertical: 15),
      padding: const EdgeInsets.all(20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Expanded(
            child: IconButton(
              onPressed: () => select(SmileyType.overjoyed),
              icon: SvgPicture.asset(
                'img/icons/smiley/face-grin-hearts-solid.svg',
                color: Color.fromARGB(
                    _moodSelect == SmileyType.overjoyed ? 255 : 100,
                    76,
                    175,
                    79),
              ),
              iconSize: 70,
              splashRadius: 1,
            ),
          ),
          Expanded(
            child: IconButton(
              onPressed: () => select(SmileyType.happy),
              icon: SvgPicture.asset(
                'img/icons/smiley/face-laugh-beam-solid.svg',
                color: Color.fromARGB(
                    _moodSelect == SmileyType.happy ? 255 : 100, 204, 220, 57),
              ),
              iconSize: 70,
              splashRadius: 1,
            ),
          ),
          Expanded(
            child: IconButton(
              onPressed: () => select(SmileyType.neutral),
              icon: SvgPicture.asset(
                'img/icons/smiley/face-frown-open-solid.svg',
                color: Color.fromARGB(
                    _moodSelect == SmileyType.neutral ? 255 : 100,
                    255,
                    235,
                    59),
              ),
              iconSize: 70,
              splashRadius: 1,
            ),
          ),
          Expanded(
            child: IconButton(
              onPressed: () => select(SmileyType.unhappy),
              icon: SvgPicture.asset(
                'img/icons/smiley/face-frown-solid.svg',
                color: Color.fromARGB(
                    _moodSelect == SmileyType.unhappy ? 255 : 100, 255, 153, 0),
              ),
              iconSize: 70,
              splashRadius: 1,
            ),
          ),
          Expanded(
            child: IconButton(
              onPressed: () => select(SmileyType.sad),
              icon: SvgPicture.asset(
                'img/icons/smiley/face-sad-tear-solid.svg',
                color: Color.fromARGB(
                    _moodSelect == SmileyType.sad ? 255 : 100, 255, 86, 34),
              ),
              iconSize: 70,
              splashRadius: 1,
            ),
          ),
        ],
      ),
    );
  }
}

enum SmileyType { overjoyed, happy, neutral, unhappy, sad }

class SmileyIcon extends StatefulWidget {
  final SmileyType type;
  final double size;

  const SmileyIcon({
    Key? key,
    required this.type,
    this.size = 70,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SmileyIconState();
}

class _SmileyIconState extends State<SmileyIcon> {
  late Color _color;
  late String _icon;

  @override
  void initState() {
    if (widget.type == SmileyType.sad) {
      _icon = 'img/icons/smiley/face-sad-tear-solid.svg';
      _color = const Color.fromARGB(255, 255, 86, 34);
    } else if (widget.type == SmileyType.unhappy) {
      _icon = 'img/icons/smiley/face-frown-solid.svg';
      _color = const Color.fromARGB(255, 255, 153, 0);
    } else if (widget.type == SmileyType.neutral) {
      _icon = 'img/icons/smiley/face-frown-open-solid.svg';
      _color = const Color.fromARGB(255, 255, 235, 59);
    } else if (widget.type == SmileyType.happy) {
      _icon = 'img/icons/smiley/face-laugh-beam-solid.svg';
      _color = const Color.fromARGB(255, 204, 220, 57);
    } else {
      _icon = 'img/icons/smiley/face-grin-hearts-solid.svg';
      _color = const Color.fromARGB(255, 76, 175, 79);
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: IconButton(
        onPressed: null,
        icon: SvgPicture.asset(
          _icon,
          color: _color,
        ),
        iconSize: widget.size,
        splashRadius: 1,
      ),
    );
  }
}
