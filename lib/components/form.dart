import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class SmilyForm extends StatelessWidget {
  final List<Widget> children;
  final GlobalKey<FormState>? formKey;

  const SmilyForm({Key? key, required this.children, this.formKey})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 45),
      child: Form(
        key: formKey,
        child: Column(
          children: children,
        ),
      ),
    );
  }
}

class SmilyTextField extends StatelessWidget {
  final String label;
  final String iconAssetName;
  final double marginTop;

  const SmilyTextField({
    Key? key,
    required this.label,
    required this.iconAssetName,
    this.marginTop = 20,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: marginTop),
      child: TextField(
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(50.0),
          ),
          labelText: label,
          prefixIcon: Align(
            widthFactor: 3,
            child: SvgPicture.asset(
              iconAssetName,
              width: 25,
              color: Colors.grey,
            ),
          ),
        ),
        style: const TextStyle(
          height: 2.2,
        ),
      ),
    );
  }
}
