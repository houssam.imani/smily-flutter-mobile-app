import 'package:awesome_notifications/awesome_notifications.dart';

int createUniqueId() {
  return DateTime.now().millisecondsSinceEpoch.remainder((100000));
}

Future<void> createReminderNotification(
    String title, String commentaire) async {
  for (int i = 1; i <= 7; i++) {
    await AwesomeNotifications().createNotification(
        content: NotificationContent(
          id: createUniqueId(),
          channelKey: 'scheduled_channel',
          title: title,
          body: commentaire,
          notificationLayout: NotificationLayout.Default,
        ),
        actionButtons: [
          NotificationActionButton(
            key: 'Mark_DONE',
            label: 'Remplir',
            buttonType: ActionButtonType.Default,
          ),
        ],
        schedule: NotificationCalendar(
          weekday: i,
          hour: 20,
          minute: 0,
          second: 0,
          millisecond: 0,
          repeats: true,
        ));
  }
}

Future<void> cancelScheduledNotifications() async {
  await AwesomeNotifications().cancelAllSchedules();
}
