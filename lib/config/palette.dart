import 'package:flutter/material.dart';

class Palette {
  static const MaterialColor smilyPurple = MaterialColor(
    0xFFB393F3, // 0%  this will be color picked if no shade is selected when defining a Color property which doesn’t require a swatch
    <int, Color>{
      50: Color(0xFFB393F3), //10%
      100: Color(0xFFB393F3), //20%
      200: Color(0xFFB393F3), //30%
      300: Color(0xFFB393F3), //40%
      400: Color(0xFFB393F3), //50%
      500: Color(0xFFB393F3), //60%
      600: Color(0xFFB393F3), //70%
      700: Color(0xFFB393F3), //80%
      800: Color(0xFFB393F3), //90%
      900: Color(0xFFB393F3), //100%
    },
  );

  static const Color smilyGrey = Color(0xff777777);

  static const Color smilyDarkGrey = Color(0xff333333);

  static const Color smilyLightGrey = Color(0xffaaaaaa);

  static const Color smilySuperLightGrey = Color(0xffF0F0F0);

  static const Color smilyWhiteGrey = Color(0xfff8f8f8);
}

