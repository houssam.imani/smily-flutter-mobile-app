import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';
import 'package:smily/components/buttons.dart';
import 'package:smily/components/form.dart';
import 'package:smily/components/smily.dart';
import 'package:smily/model/user_model.dart';
import 'package:smily/model/user_provider.dart';

class Connexion extends StatefulWidget {
  static String routename = "/connexion";
  const Connexion({Key? key}) : super(key: key);
  @override
  State<Connexion> createState() => _ConnexionState();
}

class _ConnexionState extends State<Connexion> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  late bool _secret = true;

  late User user;
  late Future<Response?> response;

  @override
  void initState() {
    user = User(
      email: " ",
      username: " ",
      password: " ",
    );
    super.initState();
  }

  void submitForm() {
    if (formKey.currentState!.validate()) {
      formKey.currentState!.save();
    }
    logUser();
  }

  void logUser() async {
    response = Provider.of<UserProvider>(context, listen: false).logUser(user);
    response.then((response) => {
          if (response?.statusCode == 200)
            {
              Session.userId = json.decode(response!.body)['userId'],
              Session.token = json.decode(response.body)['token'],
              Navigator.pushNamed(context, '/dashboard'),
            }
        });
  }

  @override
  Widget build(BuildContext context) {
    return SmilyScaffoldScroll(
      children: [
        Container(
          margin: const EdgeInsets.only(top: 25),
          child: SvgPicture.asset(
            'img/undraw/quite-town.svg',
            width: 340,
          ),
        ),
        Container(
          margin: const EdgeInsets.all(45),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                "Connexion",
                style: TextStyle(
                  fontSize: 40,
                  color: Palette.smilyDarkGrey,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 15),
                child: const Text("Connecte toi pour accéder à l'application."),
              ),
              SmilyForm(
                formKey: formKey,
                children: [
                  Container(
                    margin: const EdgeInsets.only(top: 20),
                    child: TextFormField(
                      onSaved: (value) {
                        user.username = value!;
                        user.email = value;
                      },
                      validator: (username) =>
                          username != null && username.isEmpty
                              ? "Saisissez un nom d'utilisateur ou email"
                              : null,
                      controller: usernameController,
                      keyboardType: TextInputType.name,
                      autofillHints: const [AutofillHints.username],
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(50.0),
                        ),
                        labelText: "Pseudo ou Email",
                        prefixIcon: Align(
                          widthFactor: 3,
                          child: SvgPicture.asset(
                            'img/icons/user-solid.svg',
                            width: 25,
                            color: Colors.grey,
                          ),
                        ),
                      ),
                      style: const TextStyle(
                        height: 2.2,
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 20),
                    child: TextFormField(
                      onSaved: (value) {
                        user.password = value!;
                      },
                      validator: (password) =>
                          password != null && password.isEmpty
                              ? "Saisissez un mot de passe"
                              : null,
                      controller: passwordController,
                      keyboardType: TextInputType.text,
                      autofillHints: const [AutofillHints.password],
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(50.0),
                        ),
                        labelText: "Mot de passe",
                        prefixIcon: Align(
                          widthFactor: 3,
                          child: SvgPicture.asset(
                            'img/icons/lock-solid.svg',
                            width: 25,
                            color: Colors.grey,
                          ),
                        ),
                        suffixIcon: Align(
                          widthFactor: 2.2,
                          child: InkWell(
                            onTap: () => setState(() => _secret = !_secret),
                            child: Icon(
                              !_secret
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              size: 32,
                            ),
                          ),
                        ),
                      ),
                      style: const TextStyle(
                        height: 2.2,
                      ),
                      obscureText: _secret,
                    ),
                  ),
                  SmilyButton(
                    "Valider et continuer",
                    type: ButtonType.next,
                    margin: const EdgeInsets.only(top: 25),
                    action: submitForm,
                  ),
                ],
              ),
              SmilyTextButton(
                "Mot de passe oublié ?",
                margin: const EdgeInsets.only(top: 25),
                action: () => {Navigator.pushNamed(context, '/mdp-oublie')},
              ),
              SmilyTextButton(
                "Pas encore inscrit ? Inscris toi ici !",
                color: Palette.smilyLightGrey,
                action: () => {Navigator.pushNamed(context, '/inscription')},
              ),
            ],
          ),
        ),
      ],
    );
  }
}
