import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:smily/components/buttons.dart';
import 'package:smily/components/smily.dart';

class NotificationSetup extends StatelessWidget {
  const NotificationSetup({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SmilyScaffoldScroll(
      children: [
        Container(
          margin: const EdgeInsets.only(top: 25),
          child: SvgPicture.asset(
            'img/undraw/clock.svg',
            width: 230,
          ),
        ),
        Container(
          margin: const EdgeInsets.all(45),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                "À quelle heure souhaites-tu recevoir la notification ?",
                style: TextStyle(
                  fontSize: 30,
                  color: Palette.smilyDarkGrey,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 35),
                child: const Text(
                    "Tenir un journal fonctionne mieux lorsque tu en fais une habitude. Configure l'heure à laquelle tu souhaite recevoir la petite notification pour suivre ton humeur :)"),
              ),
              Container(
                margin: const EdgeInsets.only(top: 40),
                child: Card(
                  elevation: 4,
                  child: Container(
                    margin: const EdgeInsets.all(12),
                    child: Row(
                      children: [
                        Container(
                          margin: const EdgeInsets.only(right: 25),
                          child: const Icon(
                            Icons.access_alarm,
                            color: Palette.smilyPurple,
                            size: 50,
                          ),
                        ),
                        const Text(
                          "20:00",
                          style: TextStyle(
                            color: Palette.smilyDarkGrey,
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SmilyButton(
                "Valider et continuer",
                type: ButtonType.next,
                margin: const EdgeInsets.only(top: 35),
                action: () => {Navigator.pushNamed(context, '/dashboard')},
                active: true,
              ),
              SmilyTextButton(
                "Définir l'heure plus tard",
                margin: const EdgeInsets.only(top: 25),
                action: () => {Navigator.pushNamed(context, '/dashboard')},
              ),
            ],
          ),
        ),
      ],
    );
  }
}
