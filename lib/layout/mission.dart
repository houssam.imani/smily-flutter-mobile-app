import 'package:flutter/material.dart';
import 'package:smily/components/buttons.dart';
import 'package:smily/config/palette.dart';

class Mission extends StatelessWidget {
  const Mission({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Palette.smilyPurple,
      body: SizedBox(
        height: double.infinity,
        child: Stack(
          alignment: Alignment.topCenter,
          children: [
            SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    margin:
                        const EdgeInsets.only(top: 30, left: 30, bottom: 20),
                    child: Row(
                      children: [
                        const Image(
                          image: AssetImage('img/logo-smily.png'),
                          width: 80,
                        ),
                        Container(
                          margin: const EdgeInsets.only(left: 14, bottom: 14),
                          child: const Text(
                            "Smily",
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 35,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 45),
                    alignment: Alignment.center,
                    child: Column(
                      children: const [
                        Text(
                          "Ma mission ?",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 50,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Paragraph(
                            "Tu te sens parfois un peu dépassé par tes humeurs qui te font vivre des montagnes russes ? Tu es au bon endroit ! \nJe vais t'aider à exprimer ce que tu ressens pour mieux comprendre et anticiper tes humeurs."),
                        Paragraph(
                            "Sélectionne ton humeur chaque jour dès que tu reçois la notification ou à n'importe quel autre moment de la journée. Je me charge du reste ! \nTu pourras y ajouter des notes ou des vocaux pour plus de documentations."),
                        Paragraph(
                            "Trêve de bavardage ! Appuie sur le bouton ci-dessous pour commencer.")
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Positioned(
              child: SmilyButton(
                "Poursuivre !",
                type: ButtonType.next,
                colorReverse: true,
                action: () => {Navigator.pushNamed(context, '/inscription')},
                width: 350,
                active: true,
              ),
              bottom: 90,
            ),
          ],
        ),
      ),
    );
  }
}

class Paragraph extends StatelessWidget {
  final String text;

  const Paragraph(this.text, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: 30),
      child: Text(
        text,
        textAlign: TextAlign.center,
        style: const TextStyle(
          color: Colors.white,
          fontSize: 22,
        ),
      ),
    );
  }
}
