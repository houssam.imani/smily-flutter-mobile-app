import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';
import 'package:smily/components/cards.dart';
import 'package:smily/components/form.dart';
import 'package:smily/components/smiley.dart';
import 'package:smily/components/smily.dart';
import 'package:smily/model/activity_provider.dart';
import 'package:smily/model/review_model.dart';
import 'package:smily/model/review_provider.dart';
import 'package:smily/model/text_note_provider.dart';

class Journal extends StatefulWidget {
  const Journal({Key? key}) : super(key: key);
  @override
  State<Journal> createState() => _JournalState();
}

class _JournalState extends State<Journal> {
  late Future<Response?> response;

  late List<ReviewDisplay> reviewDisplays;

  @override
  void initState() {
    Session.redirectIfNotConnected(context);
    reviewDisplays = <ReviewDisplay>[];
    populateReviews();
    super.initState();
  }

  void populateReviews() {
    List<Review> reviews = <Review>[];
    response = Provider.of<ReviewProvider>(context, listen: false)
        .fetchData(Session.userId!);
    response.then((response) => {
          for (var review in jsonDecode(response!.body))
            {
              reviews.add(Review.fromJson(review)),
            },
          reviewsToWidgets(reviews)
        });
  }

  void reviewsToWidgets(List<Review> reviews) {
    for (var review in reviews) {
      List<String> textNotes = <String>[];
      List<ActivityType> activities = <ActivityType>[];

      response = Provider.of<TextNoteProvider>(context, listen: false)
          .fetchData(review.id!);
      response.then((response) => {
            for (var textNote in jsonDecode(response!.body))
              {textNotes.add(textNote['note'])},
            Provider.of<ActivityProvider>(context, listen: false)
                .fetchData(review.id!)
                .then((response) => {
                      for (var activity in jsonDecode(response!.body))
                        {
                          activities.add(ActivityType.values.firstWhere(
                              (e) => describeEnum(e) == activity['activity']))
                        },
                      setState(() {
                        reviewDisplays.add(ReviewDisplay(
                          date: review.date,
                          time: review.time,
                          smiley: SmileyType.values.firstWhere(
                              (e) => describeEnum(e) == review.mood),
                          activities: activities,
                          textEntries: textNotes,
                        ));
                      }),
                    })
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    return SmilyScaffoldScroll(
      margin: const EdgeInsets.all(45),
      navBar: true,
      children: [
        const SmilyTextField(
          iconAssetName: 'img/icons/calendar-solid.svg',
          label: 'Choisir une période',
          marginTop: 0,
        ),
        Column(
          children: reviewDisplays,
        ),
      ],
    );
  }
}

class ReviewDisplay extends StatelessWidget {
  final String date;
  final String time;
  final SmileyType smiley;
  final List<ActivityType>? activities;
  final List<String>? textEntries;

  const ReviewDisplay({
    Key? key,
    required this.date,
    required this.time,
    required this.smiley,
    this.activities,
    this.textEntries,
  }) : super(key: key);

  List<SmilyActivityType> smilifyActivities(List<ActivityType> activities) {
    List<SmilyActivityType> smilyActivities = <SmilyActivityType>[];
    for (var activityType in activities) {
      smilyActivities.add(SmilyActivityType(type: activityType));
    }
    return smilyActivities;
  }

  List<SizedBox> smilifyTextEntries(List<String> entries) {
    List<SizedBox> smilyFlatCards = <SizedBox>[];
    for (var entry in entries) {
      smilyFlatCards.add(SizedBox(
        width: double.infinity,
        child: SmilyFlatCard(child: Text(entry)),
      ));
    }
    return smilyFlatCards;
  }

  @override
  Widget build(BuildContext context) {
    return SmilyCard(
      child: Column(
        children: [
          Row(
            children: [
              Row(
                children: [
                  SmilyBadge(
                    label: date,
                    icon: 'img/icons/calendar-solid.svg',
                  ),
                  SmilyBadge(label: time),
                  IconButton(
                    onPressed: null,
                    icon: SvgPicture.asset(
                      'img/icons/pen-to-square-solid.svg',
                      color: Palette.smilyPurple,
                    ),
                    iconSize: 40,
                    splashRadius: 1,
                  )
                ],
              ),
              SmileyIcon(
                type: smiley,
                size: 45,
              ),
            ],
          ),
          Container(
            margin: const EdgeInsets.only(top: 15),
            child: activities != null
                ? Wrap(
                    spacing: 15,
                    children: smilifyActivities(activities!),
                  )
                : null,
          ),
          Container(
            child: textEntries != null
                ? Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: smilifyTextEntries(textEntries!),
                  )
                : null,
          ),
        ],
      ),
    );
  }
}
