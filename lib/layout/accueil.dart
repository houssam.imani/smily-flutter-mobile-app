import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:smily/components/buttons.dart';
import 'package:smily/config/palette.dart';

class Accueil extends StatelessWidget {
  const Accueil({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Palette.smilyPurple,
      body: Stack(
        alignment: Alignment.center,
        children: [
          Stack(
            alignment: Alignment.bottomLeft,
            children: [
              Container(
                margin: const EdgeInsets.only(bottom: 35, left: 10),
                child: SvgPicture.asset(
                  'img/undraw/staring-window.svg',
                  width: 140,
                ),
              ),
              Container(
                margin: const EdgeInsets.symmetric(horizontal: 60),
                alignment: Alignment.center,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Column(
                      children: const [
                        Text(
                          "Bienvenue sur",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 50,
                          ),
                        ),
                        Text(
                          "Smily",
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 50,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 35),
                      child: const Text(
                        "Ton guide pour t'apprendre à mieux gérer ton humeur ;)",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 22,
                        ),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 30, bottom: 40),
                      child: const Image(
                        image: AssetImage('img/logo-smily.png'),
                        width: 260,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Positioned(
            child: SmilyButton(
              "Démarrer !",
              type: ButtonType.next,
              colorReverse: true,
              action: () => {Navigator.pushNamed(context, '/mission')},
              width: 350,
              active: true,
            ),
            bottom: 90,
          ),
        ],
      ),
    );
  }
}
