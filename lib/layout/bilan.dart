import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:intl/intl.dart';
import 'package:smily/components/buttons.dart';
import 'package:smily/components/cards.dart';
import 'package:smily/components/smiley.dart';
import 'package:smily/components/smily.dart';
import 'package:smily/model/review_model.dart';

class Bilan extends StatefulWidget {
  const Bilan({Key? key}) : super(key: key);
  @override
  State<Bilan> createState() => _BilanState();
}

class _BilanState extends State<Bilan> {
  late Review review;
  late List<String> textNotes;
  late bool buttonActive;

  final textNoteController = TextEditingController();

  @override
  void initState() {
    Session.redirectIfNotConnected(context);
    review = Review(
      userId: Session.userId!,
      date: DateFormat('dd/MM/yy').format(DateTime.now()),
      time: DateFormat('kk:mm').format(DateTime.now()),
      mood: '',
    );
    textNotes = <String>[];
    buttonActive = false;
    super.initState();
  }

  void setMood(String mood) {
    review.mood = mood;
    if (textNotes.isNotEmpty) {
      setState(() {
        buttonActive = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SizedBox(
        height: double.infinity,
        child: Stack(
          alignment: Alignment.center,
          children: [
            SingleChildScrollView(
              child: Container(
                margin: const EdgeInsets.fromLTRB(45, 45, 45, 100),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: const EdgeInsets.symmetric(horizontal: 70),
                      child: Stack(
                        alignment: AlignmentDirectional.center,
                        children: [
                          SvgPicture.asset(
                            'img/undraw/feeling.svg',
                            width: 300,
                          ),
                          Column(
                            children: [
                              const Text(
                                "Comment est-ce que tu te sens ?",
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  fontSize: 34,
                                  color: Palette.smilyDarkGrey,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.only(top: 70),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    SmilyBadge(
                                      label: review.date,
                                      icon: 'img/icons/calendar-solid.svg',
                                    ),
                                    SmilyBadge(label: review.time),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                    MoodChoice(sendMood: setMood),
                    Column(
                      children: [
                        Saisie(
                          title: "Notes écrites",
                          icon: 'img/icons/book-open-solid.svg',
                          emptyText: "Aucune note enregistrée",
                          buttonText: "Ajouter une note",
                          content: textNotes.isNotEmpty
                              ? TextNotesContent(textNotes: textNotes)
                              : null,
                          buttonAction: () {
                            showDialog(
                              context: context,
                              builder: (context) => SmilyAlertDialog(
                                children: [
                                  SizedBox(
                                    width: double.infinity,
                                    child: SmilyFlatCard(
                                      child: TextField(
                                        controller: textNoteController,
                                        autofocus: true,
                                        keyboardType: TextInputType.multiline,
                                        textInputAction:
                                            TextInputAction.newline,
                                        minLines: 16,
                                        maxLines: 24,
                                        decoration: const InputDecoration(
                                          border: InputBorder.none,
                                          contentPadding: EdgeInsets.all(0),
                                        ),
                                        style: const TextStyle(
                                          fontFamily: "Roboto",
                                          fontSize: 18,
                                          color: Palette.smilyGrey,
                                        ),
                                      ),
                                    ),
                                  ),
                                  SmilyButton(
                                    "Enregistrer la saisie",
                                    type: ButtonType.save,
                                    margin: const EdgeInsets.only(top: 20),
                                    action: () {
                                      setState(() {
                                        textNotes.add(textNoteController.text);
                                        textNoteController.text = '';
                                        if (review.mood != '') {
                                          buttonActive = true;
                                        }
                                      });
                                      Navigator.pop(context);
                                    },
                                  )
                                ],
                              ),
                            );
                          },
                        ),
                        const Saisie(
                          title: "Enregistrements vocaux",
                          icon: 'img/icons/volume-high-solid.svg',
                          buttonText: "Ajouter un vocal",
                          emptyText: "Aucun vocal enregistré",
                        ),
                        const Saisie(
                          title: "Photos de la journée",
                          icon: 'img/icons/book-open-solid.svg',
                          buttonText: "Ajouter une photo",
                          emptyText: "Aucune photo enregistrée",
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            SmilyBottomButton(
              "Continuer",
              active: buttonActive,
              action: () => {
                Session.review = review,
                Session.textNotes = textNotes,
                Navigator.pushNamed(context, '/bilan-icons'),
              },
            ),
          ],
        ),
      ),
    );
  }
}

class Saisie extends StatelessWidget {
  final String title;
  final String icon;
  final String buttonText;
  final String emptyText;
  final Function()? buttonAction;
  final Widget? content;

  const Saisie({
    Key? key,
    required this.title,
    required this.icon,
    required this.buttonText,
    required this.emptyText,
    this.buttonAction,
    this.content,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(vertical: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: const EdgeInsets.only(bottom: 15),
            child: Row(
              children: [
                Container(
                  margin: const EdgeInsets.only(right: 15),
                  child: SvgPicture.asset(
                    icon,
                    width: 30,
                  ),
                ),
                Text(
                  title,
                  style: const TextStyle(
                    color: Palette.smilyDarkGrey,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
          Container(
            child: content ?? Text(emptyText),
          ),
          SmilyTextButton(
            buttonText,
            alignment: Alignment.topLeft,
            action: buttonAction,
          ),
        ],
      ),
    );
  }
}

class TextNotesContent extends StatelessWidget {
  final List<String> textNotes;

  const TextNotesContent({Key? key, required this.textNotes}) : super(key: key);

  List<SizedBox> getTextNotesContent() {
    List<SizedBox> smilyTextNotes = <SizedBox>[];
    for (var textNote in textNotes) {
      smilyTextNotes.add(SizedBox(
        width: double.infinity,
        child: SmilyCard(
          color: Palette.smilyWhiteGrey,
          elevation: 0,
          child: Text(textNote),
          margin: const EdgeInsets.only(bottom: 10),
          padding: const EdgeInsets.all(20),
        ),
      ));
    }
    return smilyTextNotes;
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: getTextNotesContent(),
    );
  }
}
