import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:smily/components/buttons.dart';
import 'package:smily/components/form.dart';
import 'package:smily/components/smily.dart';

class MdpOublie extends StatelessWidget {
  const MdpOublie({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SmilyScaffoldScroll(
      children: [
        Container(
          margin: const EdgeInsets.only(top: 25),
          child: SvgPicture.asset(
            'img/undraw/forgot-password.svg',
            width: 300,
          ),
        ),
        Container(
          margin: const EdgeInsets.all(45),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                "Mot de passe oublié",
                style: TextStyle(
                  fontSize: 40,
                  color: Palette.smilyDarkGrey,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 10),
                child: const Text(
                  "Pas de panique ! Je me charge de tout ;)",
                  style: TextStyle(
                    color: Palette.smilyDarkGrey,
                  ),
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 35),
                child: const Text(
                    "Renseigne ici ton adresse mail et un lien te sera envoyé pour réinitialiser ton mot de passe."),
              ),
              const SmilyForm(
                children: [
                  SmilyTextField(
                      label: "Email",
                      iconAssetName: 'img/icons/envelope-solid.svg'),
                  SmilyButton(
                    "Envoyer",
                    type: ButtonType.send,
                    margin: EdgeInsets.only(top: 25),
                    active: true,
                  )
                ],
              ),
              SmilyTextButton(
                "J'ai retrouvé mon mot de passe",
                margin: const EdgeInsets.only(top: 25),
                action: () => {Navigator.pushNamed(context, '/connexion')},
              ),
            ],
          ),
        ),
      ],
    );
  }
}
