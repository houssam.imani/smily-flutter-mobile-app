import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:http/http.dart';
import 'package:provider/provider.dart';
import 'package:smily/components/buttons.dart';
import 'package:smily/components/cards.dart';
import 'package:smily/components/smily.dart';
import 'package:smily/model/activity_model.dart';
import 'package:smily/model/activity_provider.dart';
import 'package:smily/model/review_model.dart';
import 'package:smily/model/review_provider.dart';
import 'package:smily/model/text_note_model.dart';
import 'package:smily/model/text_note_provider.dart';

class BilanIcons extends StatefulWidget {
  const BilanIcons({Key? key}) : super(key: key);
  @override
  State<BilanIcons> createState() => _BilanIconsState();
}

class _BilanIconsState extends State<BilanIcons> {
  late Review review;
  late List<String> textNotes;
  late List<String> activities;
  late Future<Response?> response;

  @override
  void initState() {
    if (Session.review == null) {
      Future.delayed(Duration.zero, () {
        Navigator.pushNamed(context, '/bilan');
      });
    } else {
      review = Session.review!;
      textNotes = Session.textNotes!;
    }

    activities = <String>[];
    super.initState();
  }

  void validateReview() {
    response =
        Provider.of<ReviewProvider>(context, listen: false).addReview(review);
    response.then((response) => {
          if (response?.statusCode == 201)
            {
              review.id = json.decode(response!.body)['reviewId'],
              for (var activity in activities)
                {
                  Provider.of<ActivityProvider>(context, listen: false)
                      .addActivity(
                          Activity(reviewId: review.id!, activity: activity))
                },
              for (var textNote in textNotes)
                {
                  Provider.of<TextNoteProvider>(context, listen: false)
                      .addTextNote(
                          TextNote(reviewId: review.id!, note: textNote))
                },
              Session.reviewFlush(),
              Navigator.pushNamed(context, '/journal'),
            }
        });
  }

  void addActivity(String activity) {
    if (activities.contains(activity)) {
      activities.remove(activity);
    } else {
      activities.add(activity);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SizedBox(
        height: double.infinity,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Positioned(
              top: -40,
              right: 0,
              child: SvgPicture.asset(
                'img/undraw/health.svg',
                width: 350,
              ),
            ),
            Positioned(
              bottom: 90,
              left: 0,
              child: SvgPicture.asset(
                'img/undraw/feeling.svg',
                width: 330,
              ),
            ),
            SingleChildScrollView(
              child: Container(
                margin: const EdgeInsets.fromLTRB(45, 45, 45, 100),
                child: Column(
                  children: [
                    SmilySectionCard(
                      title: "Santé",
                      child: Wrap(
                        spacing: 20,
                        runSpacing: 15,
                        children: [
                          SmilyActivityIcon(
                            type: ActivityType.malade,
                            sendActivity: addActivity,
                          ),
                          SmilyActivityIcon(
                            type: ActivityType.sport,
                            sendActivity: addActivity,
                          ),
                          SmilyActivityIcon(
                            type: ActivityType.meditation,
                            sendActivity: addActivity,
                          ),
                          SmilyActivityIcon(
                            type: ActivityType.marche,
                            sendActivity: addActivity,
                          ),
                        ],
                      ),
                    ),
                    SmilySectionCard(
                      title: "Lieu",
                      child: Wrap(
                        spacing: 20,
                        runSpacing: 15,
                        children: [
                          SmilyActivityIcon(
                            type: ActivityType.maison,
                            sendActivity: addActivity,
                          ),
                          SmilyActivityIcon(
                            type: ActivityType.travail,
                            sendActivity: addActivity,
                          ),
                          SmilyActivityIcon(
                            type: ActivityType.hopital,
                            sendActivity: addActivity,
                          ),
                          SmilyActivityIcon(
                            type: ActivityType.balade,
                            sendActivity: addActivity,
                          ),
                          SmilyActivityIcon(
                            type: ActivityType.salleSport,
                            sendActivity: addActivity,
                          ),
                          SmilyActivityIcon(
                            type: ActivityType.cinema,
                            sendActivity: addActivity,
                          ),
                          SmilyActivityIcon(
                            type: ActivityType.plage,
                            sendActivity: addActivity,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SmilyBottomButton(
              "Valider",
              action: validateReview,
              type: ButtonType.commit,
            ),
          ],
        ),
      ),
    );
  }
}
