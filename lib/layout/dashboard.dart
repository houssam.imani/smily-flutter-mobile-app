import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:smily/components/smily.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);
  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  void initState() {
    Session.redirectIfNotConnected(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SmilyScaffoldScroll(
      margin: const EdgeInsets.all(20),
      navBar: true,
      children: [
        Container(
          margin: const EdgeInsets.only(bottom: 15),
          child: Card(
            elevation: 5,
            child: InkWell(
              onTap: () => {Navigator.pushNamed(context, '/bilan')},
              child: Row(
                textDirection: TextDirection.ltr,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin: const EdgeInsets.all(4),
                    padding: const EdgeInsets.fromLTRB(30, 10, 10, 10),
                    width: 260,
                    child: Text(
                      "Remplis ton bilan d'aujourd'hui !",
                      style: GoogleFonts.roboto(
                        textStyle: const TextStyle(
                          fontFamily: "Roboto",
                          fontSize: 24,
                          fontWeight: FontWeight.w900,
                          height: 1.5,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.all(4),
                    child: SvgPicture.asset(
                      'img/undraw/visualization.svg',
                      width: 120,
                      height: 120,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(bottom: 10),
          child: InkWell(
            onTap: () => {Navigator.pushNamed(context, '/journal')},
            child: Card(
              elevation: 5,
              color: const Color(0xFFB393F3),
              child: Row(
                textDirection: TextDirection.ltr,
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Container(
                    margin: const EdgeInsets.all(4),
                    padding: const EdgeInsets.fromLTRB(30, 20, 10, 20),
                    width: 400,
                    child: Text(
                      "Retrouve toutes les humeurs que tu as déjà renseignés",
                      style: GoogleFonts.roboto(
                        textStyle: const TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w900,
                          fontSize: 25,
                          height: 1.5,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.fromLTRB(4, 4, 15, 4),
                    child: SvgPicture.asset(
                      'img/icons/angle-right-solid.svg',
                      width: 20,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.fromLTRB(5, 10, 5, 10),
          child: Row(
            textDirection: TextDirection.ltr,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                "Statistiques",
                style: GoogleFonts.roboto(
                  textStyle: const TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.w900,
                  ),
                ),
              ),
              const Image(
                image: AssetImage('img/logo-smily.png'),
                width: 40,
              ),
            ],
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 5, bottom: 5),
          child: InkWell(
            onTap: null,
            child: Card(
              elevation: 5,
              child: Container(
                height: 150,
              ),
            ),
          ),
        ),
        Container(
          margin: const EdgeInsets.only(top: 5, bottom: 5),
          child: InkWell(
            onTap: null,
            child: Card(
              elevation: 5,
              child: Container(
                height: 150,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
