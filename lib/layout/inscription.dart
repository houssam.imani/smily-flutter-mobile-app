import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:http/http.dart';
import 'package:smily/components/buttons.dart';
import 'package:smily/components/form.dart';
import 'package:smily/components/smily.dart';
import 'package:email_validator/email_validator.dart';
import 'package:provider/provider.dart';
import 'package:smily/layout/all_layout.dart';
import 'package:smily/model/user_model.dart';
import 'package:smily/model/user_provider.dart';

import '../components/form.dart';

class Inscription extends StatefulWidget {
  static String routename = "/inscription";
  const Inscription({Key? key}) : super(key: key);
  @override
  State<Inscription> createState() => _InscriptionState();
}

class _InscriptionState extends State<Inscription> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  final emailController = TextEditingController();
  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  late bool _secret = true;

  late User newUser;
  late Future<Response?> response;

  @override
  void initState() {
    newUser = User(
      email: " ",
      username: " ",
      password: " ",
    );
    super.initState();
  }

  void submitForm() {
    if (formKey.currentState!.validate()) {
      formKey.currentState!.save();
      saveUser();
    }
  }

  void saveUser() async {
    response =
        Provider.of<UserProvider>(context, listen: false).addUser(newUser);
    response.then((response) => {
          if (response?.statusCode == 201)
            {
              Navigator.pushNamed(context, Connexion.routename),
              ScaffoldMessenger.of(context)
                ..removeCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    content:
                        Text("L'utilisateur ${newUser.email} a été ajouté !"),
                  ),
                ),
            }
        });
  }

  @override
  Widget build(BuildContext context) {
    return SmilyScaffoldScroll(
      children: [
        Stack(
          children: [
            Align(
              alignment: Alignment.topLeft,
              child: SvgPicture.asset(
                'img/undraw/tree-swing.svg',
                width: 320,
              ),
            ),
            Align(
              alignment: Alignment.bottomRight,
              heightFactor: 1.1,
              child: SvgPicture.asset(
                'img/undraw/walk-in-nature.svg',
                width: 280,
              ),
            ),
          ],
        ),
        Container(
          margin: const EdgeInsets.all(45),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                "Inscription",
                style: TextStyle(
                  fontSize: 40,
                  color: Palette.smilyDarkGrey,
                  fontWeight: FontWeight.bold,
                ),
              ),
              Container(
                margin: const EdgeInsets.only(top: 15),
                child: const Text(
                    "L'aventure commence ici ! Crée un compte pour sauvegarder tes données personnelles."),
              ),
              SmilyForm(
                formKey: formKey,
                children: [
                  Container(
                    margin: const EdgeInsets.only(top: 20),
                    child: TextFormField(
                      onSaved: (value) {
                        newUser.email = value!.toLowerCase();
                      },
                      validator: (email) =>
                          email != null && !EmailValidator.validate(email)
                              ? 'Saisissez un email valide'
                              : null,
                      controller: emailController,
                      keyboardType: TextInputType.emailAddress,
                      autofillHints: const [AutofillHints.email],
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(50.0),
                        ),
                        labelText: 'Email',
                        prefixIcon: Align(
                          widthFactor: 3,
                          child: SvgPicture.asset(
                            'img/icons/envelope-solid.svg',
                            width: 25,
                            color: Colors.grey,
                          ),
                        ),
                      ),
                      style: const TextStyle(
                        height: 2.2,
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 20),
                    child: TextFormField(
                      onSaved: (value) {
                        newUser.username = value!;
                      },
                      validator: (username) => username != null &&
                              (username.length < 3)
                          ? "Saisissez un nom d'utilisateur de plus de 3 caractères"
                          : null,
                      controller: usernameController,
                      keyboardType: TextInputType.name,
                      autofillHints: const [AutofillHints.username],
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(50.0),
                        ),
                        labelText: 'Pseudo',
                        prefixIcon: Align(
                          widthFactor: 3,
                          child: SvgPicture.asset(
                            'img/icons/user-solid.svg',
                            width: 25,
                            color: Colors.grey,
                          ),
                        ),
                      ),
                      style: const TextStyle(
                        height: 2.2,
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 20),
                    child: TextFormField(
                      onSaved: (value) {
                        newUser.password = value!;
                      },
                      validator: (password) => password != null &&
                              (password.length < 8)
                          ? 'Saisissez un mot de passe de plus de 8 caractères'
                          : null,
                      controller: passwordController,
                      keyboardType: TextInputType.text,
                      autofillHints: const [AutofillHints.password],
                      decoration: InputDecoration(
                        suffixIcon: Align(
                          widthFactor: 2.2,
                          child: InkWell(
                            onTap: () => setState(() => _secret = !_secret),
                            child: Icon(
                              !_secret
                                  ? Icons.visibility
                                  : Icons.visibility_off,
                              size: 32,
                            ),
                          ),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(50.0),
                        ),
                        labelText: 'Mot de passe',
                        prefixIcon: Align(
                          widthFactor: 3,
                          child: SvgPicture.asset(
                            'img/icons/lock-solid.svg',
                            width: 25,
                            color: Colors.grey,
                          ),
                        ),
                      ),
                      style: const TextStyle(
                        height: 2.2,
                      ),
                      obscureText: _secret,
                    ),
                  ),
                  SmilyButton(
                    "Valider et continuer",
                    type: ButtonType.next,
                    margin: const EdgeInsets.only(top: 25),
                    action: submitForm,
                  ),
                ],
              ),
              SmilyTextButton(
                "Déjà un compte ? Connecte toi ici !",
                margin: const EdgeInsets.only(top: 25),
                color: Palette.smilyLightGrey,
                action: () => {Navigator.pushNamed(context, '/connexion')},
              ),
            ],
          ),
        ),
      ],
    );
  }
}
