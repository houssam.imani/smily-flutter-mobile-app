import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:smily/components/cards.dart';
import 'package:smily/components/smily.dart';

class Parametres extends StatefulWidget {
  const Parametres({Key? key}) : super(key: key);
  @override
  State<Parametres> createState() => _ParametresState();
}

class _ParametresState extends State<Parametres> {
  @override
  void initState() {
    Session.redirectIfNotConnected(context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SmilyScaffoldScroll(
      margin: const EdgeInsets.all(45),
      navBar: true,
      children: [
        Container(
          margin: const EdgeInsets.only(bottom: 40),
          child: Row(
            children: [
              Container(
                margin: const EdgeInsets.only(right: 10),
                child: const Icon(
                  Icons.settings,
                  color: Palette.smilyDarkGrey,
                  size: 40,
                ),
              ),
              const Text(
                "Paramètres",
                style: TextStyle(
                  color: Palette.smilyDarkGrey,
                  fontWeight: FontWeight.bold,
                  fontSize: 30,
                ),
              ),
            ],
          ),
        ),
        const Align(
          alignment: Alignment.topLeft,
          child: Text(
            "Changer l'heure de la notification",
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
        ),
        SmilyCard(
          margin: const EdgeInsets.only(top: 25, bottom: 35),
          padding: const EdgeInsets.all(10),
          child: Row(
            children: [
              Container(
                margin: const EdgeInsets.only(right: 25),
                child: const Icon(
                  Icons.access_alarm,
                  color: Palette.smilyPurple,
                  size: 50,
                ),
              ),
              const Text(
                "20:00",
                style: TextStyle(
                  color: Palette.smilyDarkGrey,
                  fontWeight: FontWeight.bold,
                  fontSize: 20,
                ),
              ),
            ],
          ),
        ),
        Column(
          children: [
            Container(
              decoration: const BoxDecoration(
                border: Border(
                  bottom: BorderSide(color: Palette.smilyLightGrey),
                ),
              ),
            ),
            Container(
              decoration: const BoxDecoration(
                border: Border(
                  bottom: BorderSide(color: Palette.smilyLightGrey),
                ),
              ),
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: Row(
                children: [
                  Container(
                    margin: const EdgeInsets.only(right: 25),
                    child: SvgPicture.asset(
                      'img/icons/children-solid.svg',
                      color: Palette.smilyPurple,
                      width: 25,
                    ),
                  ),
                  const Text("Conditions générales d'utilisation"),
                ],
              ),
            ),
            Container(
              decoration: const BoxDecoration(
                border: Border(
                  bottom: BorderSide(color: Palette.smilyLightGrey),
                ),
              ),
              padding: const EdgeInsets.symmetric(vertical: 20),
              child: Row(
                children: [
                  Container(
                    margin: const EdgeInsets.only(right: 25),
                    child: SvgPicture.asset(
                      'img/icons/lock-solid.svg',
                      color: Palette.smilyPurple,
                      width: 20,
                    ),
                  ),
                  const Text("Politique de confidentialité"),
                ],
              ),
            ),
          ],
        ),
        Container(
          margin: const EdgeInsets.only(top: 35),
          alignment: Alignment.topLeft,
          child: ElevatedButton(
            onPressed: () {
              Session.flush();
              Navigator.pushNamed(context, '/connexion');
            },
            style: ButtonStyle(
              fixedSize: MaterialStateProperty.all<Size>(const Size(250, 70)),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
              ),
              backgroundColor: MaterialStateProperty.all<Color>(Colors.red),
            ),
            child: Container(
              margin: const EdgeInsets.all(5),
              child: Row(
                children: [
                  Container(
                    margin: const EdgeInsets.only(right: 15),
                    child: const Icon(
                      Icons.power_settings_new,
                      color: Colors.white,
                      size: 35,
                    ),
                  ),
                  const Text(
                    'Déconnexion',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 22,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
