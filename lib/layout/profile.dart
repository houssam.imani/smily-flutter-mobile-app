import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:smily/components/form_profile.dart';
import 'package:smily/components/smily.dart';
import 'package:smily/components/buttons.dart';
import 'package:intl/intl.dart' as intl;
import 'package:smily/model/user_provider.dart';
import 'package:http/http.dart';
import 'package:image_picker/image_picker.dart';

import '../model/user_model.dart';

class Profile extends StatefulWidget {
  const Profile({Key? key}) : super(key: key);
  @override
  State<Profile> createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  final usernameController = TextEditingController();
  final passwordController = TextEditingController();
  final passwordConfirmationController = TextEditingController();

  late Future<Response?> response;
  late User userSession;

  String? username;
  DateTime? birthdate;
  String? password;
  String? passwordConfirmation;

  /// Importation photo de profile
  File? image;
  Future pickImage(ImageSource source) async {
    try {
      final image = await ImagePicker().pickImage(source: source);
      if (image == null) return;

      final imageTemporary = File(image.path);
      setState(() => this.image = imageTemporary);
    } on PlatformException catch (e) {
      print(e);
      rethrow;
    }
  }

  @override
  void initState() {
    Session.redirectIfNotConnected(context);

    super.initState();
  }

  void setDate() {
    showDatePicker(
      context: context,
      firstDate: DateTime(1900),
      initialDate: DateTime(1990),
      lastDate: DateTime.now(),
    ).then((pickDate) {
      if (pickDate != null) {
        setState(() {
          birthdate = pickDate;
        });
      }
    });
  }

  void submitForm() {
    if (formKey.currentState!.validate()) {
      formKey.currentState!.save();
      updateUser();
    }
  }

  void updateUser() async {
    response = Provider.of<UserProvider>(context, listen: false)
        .updateUser(userSession);
    response.then((response) => {
          if (response?.statusCode == 201)
            {
              ScaffoldMessenger.of(context)
                ..removeCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(
                    content: Text(
                        "L'utilisateur ${userSession.email} a été modifié !"),
                  ),
                ),
            }
        });
  }

  @override
  Widget build(BuildContext context) {
    Provider.of<UserProvider>(context).fetchData();
    userSession = Provider.of<UserProvider>(context).user;

    return SmilyScaffoldScroll(navBar: true, children: [
      Container(
        margin: const EdgeInsets.all(35),
        child: Column(
          textDirection: TextDirection.ltr,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Row(
              textDirection: TextDirection.ltr,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Stack(
                  children: [
                    Container(
                      margin: const EdgeInsets.only(bottom: 10),
                      child: image == null
                          ? const CircleAvatar(
                              radius: 36,
                              backgroundImage: AssetImage(
                                'img/photos/users/default.png',
                              ))
                          : CircleAvatar(
                              radius: 36,
                              backgroundImage: AssetImage(
                                image!.path,
                              )),
                    ),
                    Positioned(
                        left: 4,
                        bottom: 0,
                        child: InkWell(
                          onTap: () => {
                            showModalBottomSheet(
                                context: context,
                                builder: ((builder) => bottomSheet()))
                          },
                          child: const CircleAvatar(
                              radius: 15,
                              backgroundColor: Colors.white,
                              child: CircleAvatar(
                                radius: 12,
                                backgroundColor: Colors.pink,
                                child: Icon(
                                  Icons.add,
                                  color: Colors.white,
                                  size: 15,
                                ),
                              )),
                        )),
                  ],
                ),
                Column(
                  textDirection: TextDirection.ltr,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      margin: const EdgeInsets.only(top: 7),
                      child: Text(
                        '${userSession.username}',
                        style: const TextStyle(
                            color: Palette.smilyGrey,
                            fontWeight: FontWeight.w600,
                            fontSize: 24),
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 3),
                      child: const Text(
                        "Imani",
                        style: TextStyle(
                            color: Palette.smilyGrey,
                            fontWeight: FontWeight.w400,
                            fontSize: 20),
                      ),
                    )
                  ],
                ),
                Image.asset(
                  'img/logo-smily.png',
                  width: 55,
                ),
              ],
            ),
            SmilyProfileForm(formKey: formKey, children: [
              Container(
                  margin: const EdgeInsets.only(top: 6, bottom: 6),
                  child: Row(
                      textDirection: TextDirection.ltr,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width * 0.70,
                          margin: const EdgeInsets.only(bottom: 5),
                          padding: const EdgeInsets.fromLTRB(10, 18, 15, 18),
                          color: Palette.smilySuperLightGrey,
                          child: Text(
                            (birthdate == null)
                                ? "Choisi une date"
                                : intl.DateFormat('dd/MM/yyyy')
                                    .format(birthdate!),
                            style: const TextStyle(fontSize: 16),
                          ),
                        ),
                        ElevatedButton(
                          child: const Padding(
                            padding: EdgeInsets.fromLTRB(5, 13.5, 5, 13.5),
                            child: Icon(
                              Icons.calendar_today,
                              color: Colors.white,
                              size: 24,
                            ),
                          ),
                          onPressed: setDate,
                        ),
                      ])),
              Container(
                margin: const EdgeInsets.only(top: 6, bottom: 6),
                child: TextFormField(
                    onSaved: (value) {
                      userSession.username = value!;
                    },
                    controller: usernameController,
                    validator: (username) => username != null &&
                            (username.length < 3)
                        ? "Saisissez un nom d'utilisateur de plus de 3 caractères"
                        : null,
                    decoration: const InputDecoration(
                      border: InputBorder.none,
                      filled: true,
                      fillColor: Palette.smilySuperLightGrey,
                      labelText: "Nom d'utilisateur",
                    ),
                    style: const TextStyle(
                      color: Palette.smilyDarkGrey,
                      height: 1.5,
                    )),
              ),
              Container(
                height: 40,
              ),
              const SmilyProfileLabel(label: "Modifier le mot de passe"),
              Container(
                margin: const EdgeInsets.only(top: 6, bottom: 6),
                child: TextFormField(
                    onSaved: (value) => password = value,
                    controller: passwordController,
                    validator: (password) {
                      if (password!.isEmpty || password.length < 8) {
                        return "Entrez un mot de passe correcte";
                      } else {
                        return null;
                      }
                    },
                    decoration: const InputDecoration(
                      border: InputBorder.none,
                      filled: true,
                      fillColor: Palette.smilySuperLightGrey,
                      labelText: 'Nouveau mot de passe',
                    ),
                    obscureText: true,
                    style: const TextStyle(
                      color: Palette.smilyDarkGrey,
                      height: 1.5,
                    )),
              ),
              const SmilyProfileLabel(label: "Confirmer le mot de passe"),
              Container(
                margin: const EdgeInsets.only(top: 6, bottom: 6),
                child: TextFormField(
                    onSaved: (value) => passwordConfirmation = value,
                    controller: passwordConfirmationController,
                    validator: (passwordConfirmation) {
                      if (passwordConfirmation!.isEmpty ||
                          passwordConfirmation.length < 8) {
                        return "Entrez un mot de passe correcte";
                      } else {
                        if (passwordConfirmation != passwordController.text) {
                          return "Mot de passe de confirmation différent du mot de passe saisi";
                        } else {
                          return null;
                        }
                      }
                    },
                    decoration: const InputDecoration(
                      border: InputBorder.none,
                      filled: true,
                      fillColor: Palette.smilySuperLightGrey,
                      labelText: 'Confirmation nouveau mot de passe',
                    ),
                    obscureText: true,
                    style: const TextStyle(
                      color: Palette.smilyDarkGrey,
                      height: 1.5,
                    )),
              ),
              SmilyButton(
                "Valider",
                type: ButtonType.send,
                margin: const EdgeInsets.only(top: 25),
                action: submitForm,
              )
            ])
          ],
        ),
      ),
    ]);
  }

  bottomSheet() {
    return SizedBox(
        height: 200.0,
        width: MediaQuery.of(context).size.width,
        child: Column(
          textDirection: TextDirection.ltr,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            InkWell(
              onTap: () => pickImage(ImageSource.gallery),
              child: Container(
                decoration: const BoxDecoration(
                  border: Border(
                    bottom: BorderSide(color: Palette.smilySuperLightGrey),
                  ),
                ),
                padding: const EdgeInsets.fromLTRB(25, 35, 10, 35),
                child: Row(
                  children: [
                    Container(
                      padding: const EdgeInsets.only(right: 8),
                      child: const Icon(
                        Icons.image,
                        color: Palette.smilyPurple,
                        size: 25,
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.only(right: 8),
                      child: const Text("Gallerie",
                          style: TextStyle(
                            fontSize: 18,
                          )),
                    )
                  ],
                ),
              ),
            ),
            InkWell(
              onTap: () => pickImage(ImageSource.camera),
              child: Container(
                decoration: const BoxDecoration(
                  border: Border(
                    bottom: BorderSide(color: Palette.smilySuperLightGrey),
                  ),
                ),
                padding: const EdgeInsets.fromLTRB(25, 35, 10, 35),
                child: Row(
                  textDirection: TextDirection.ltr,
                  children: [
                    Container(
                        padding: const EdgeInsets.only(right: 8),
                        child: const Icon(
                          Icons.camera,
                          color: Palette.smilyPurple,
                          size: 25,
                        )),
                    Container(
                      padding: const EdgeInsets.only(right: 8),
                      child: const Text("Camera",
                          style: TextStyle(
                            fontSize: 18,
                          )),
                    )
                  ],
                ),
              ),
            )
          ],
        ));
  }

/*
  void takePhoto(ImageSource source) async {
    final pickedFile = await _picker.pickImage(
        source: source,
    );
    setState(() {
      _imageFile = pickedFile;
    });
  }
  */

}
