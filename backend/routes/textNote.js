const express = require('express');

const router = express.Router();
const textNoteCtrl = require('../controllers/textNote');

// Routes
router.get('/review/:reviewId', textNoteCtrl.getAllTextNotesByReview);
router.post('/', textNoteCtrl.addTextNote);

module.exports = router;