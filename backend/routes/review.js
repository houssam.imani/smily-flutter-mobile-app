const express = require('express');

const router = express.Router();
const reviewCtrl = require('../controllers/review');

// Routes
router.get('/user/:userId', reviewCtrl.getAllReviewByUser);
router.get('/:id', reviewCtrl.getOneReview);
router.post('/', reviewCtrl.createReview);

module.exports = router;