const express = require('express');

const router = express.Router();
const activityCtrl = require('../controllers/activity');

// Routes
router.get('/review/:reviewId', activityCtrl.getAllActivitiesByReview);
router.post('/', activityCtrl.addActivity);

module.exports = router;