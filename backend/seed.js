const mongoose = require("mongoose");
const User = require("./models/user.model");

mongoose
  .connect(
    "mongodb+srv://root:pass@smily.gcdvk.mongodb.net/?retryWrites=true&w=majority",
    {
      useNewUrlParser: true
    }
  )
  .then(() => {
    Promise.all([
      new User({
        email: 'Olivia@gmail.com',
        username: 'Olivia',
        password: 'Azerty,1',
      }).save(),
      new User({
        email: 'Hugo@gmail.com',
        username: 'Hugo',
        password: 'Azerty,1',
      }).save(),
      new User({
        email: 'Mathieu@gmail.com',
        username: 'Mathieu',
        password: 'Azerty,1',
      }).save(),
      
    ]).then(res => {
      console.log("data installed");
      mongoose.connection.close();
    });
  });
