const mongoose = require("mongoose");
const schema = mongoose.Schema;

const activitySchema = schema({
  reviewId: { type: String, required: true },
  activity: { type: String, required: true },
});

const Smily = mongoose.model("activity", activitySchema);

module.exports = Smily;

