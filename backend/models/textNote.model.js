const mongoose = require("mongoose");
const schema = mongoose.Schema;

const textNoteSchema = schema({
  reviewId: { type: String, required: true },
  note: { type: String, required: true },
});

const Smily = mongoose.model("textNote", textNoteSchema);

module.exports = Smily;

