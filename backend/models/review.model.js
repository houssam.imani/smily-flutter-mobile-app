const mongoose = require("mongoose");
const schema = mongoose.Schema;

const reviewSchema = schema({
  userId: { type: String, required: true },
  date: { type: String, required: true },
  time: { type: String, required: true },
  mood: { type: String, required: true },
});

const Smily = mongoose.model("review", reviewSchema);

module.exports = Smily;

