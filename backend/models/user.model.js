const mongoose = require("mongoose");
const schema = mongoose.Schema;

const userSchema = schema({
  email: { type: String, required: true, unique: true},
  username: { type: String, required: true, unique: true},
  password: { type: String, required: true },
  photo: { type: String, required: false },
  birthday: { type: Date, required: false },
  firstname: { type: String, required: false },
  lastname: { type: String, required: false },
  sex: { type: Number, required: false },
});

const Smily = mongoose.model("user", userSchema);

module.exports = Smily;

