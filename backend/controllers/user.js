const User = require('../models/user.model');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');



exports.getOneUser = (req, res, next) => {
  User.findOne({
    _id: req.params.id
  }).then(
    (user) => {
      res.status(200).json(user);
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};

exports.getAllUsers = (req, res, next) => {
  User.find().then(
    (users) => {
      res.status(200).json(users);
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};

exports.createUser = (req, res, next) => {
  bcrypt.hash(req.body.password, 10)
    .then(hash => {
      const user = new User({
        email: req.body.email,
        password: hash,
      });
      user.save()
        .then(() => res.status(201).json({ message: 'Nouvel utilisateur créé' })).
        catch(error => res.status(400).json({ error: error }));
    })
    .catch(error => res.status(500).json({ error: error }));
};

exports.createUser = (req, res, next) => {
  bcrypt.hash(req.body.password, 10)
    .then(hash => {
      const user = new User({
        email: req.body.email,
        username: req.body.username,
        password: hash,
      });
      user.save()
        .then(() => res.status(201).json({ message: 'Nouvel utilisateur créé' })).
        catch(error => res.status(400).json({ error: error }));
    })
    .catch(error => res.status(500).json({ error: error }));
};

exports.login = (req, res, next) => {
  User.findOne({ $or: [{ email: req.body.email }, { username: req.body.username }] })
    .then(user => {
      if (!user) {
        return res.status(401).json({ error: "Le nom d'utilisateur ou le mot de passe est incorrect" });
      }
      bcrypt.compare(req.body.password, user.password)
        .then(valid => {
          if (!valid) {
            return res.status(401).json({ error: "Le nom d'utilisateur ou le mot de passe est incorrect" });
          }
          res.status(200).json({
            userId: user._id,
            token: jwt.sign(
              { userId: user._id },
              'RANDOM_TOKEN_SECRET',
              { expiresIn: '24h' }
            )
          });
        })
        .catch(error => res.status(500).json({ error }));
    })
    .catch(error => res.status(500).json({ error }));
};



exports.updateUser = async (req, res) => {
    try {
        const user = await User.findById(req.params.id);
        Object.assign(user, req.body);
        user.save();
        res.send({data: user});
    } catch{
        res.status(404).send({error: "User is not found"});
    }
};





/*
exports.updateUser = (req, res, next) => {
    const filter = {
        id: req.body.id,
    }

    const updateProfileData = {
        username: req.body.username,
    }

    const dataItem = User.updateOne(filter, updateProfileData, {
        new: true,
    })
    .then((data) =>
        res.json({
            data: data,
        })
    )
    .catch((error) => {
        return res.send(error);
    });

};



exports.updateUser = (req, res, next) => {

    const filter = { _id: req.params.id };
    const update = { age: 59 };

     User.findByIdAndUpdate(filter, update)
     .then(() => res.status(201).json({ message: 'Nouvel utilisateur créé' }))
     .catch(error => res.status(500).json({ error }));

};*/