const TextNote = require('../models/textNote.model');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');


exports.getAllTextNotesByReview = (req, res, next) => {
  TextNote.find({
    reviewId: req.params.reviewId
  }).then(
    (textNotes) => {
      res.status(200).json(textNotes);
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};

exports.addTextNote = (req, res, next) => {
  const textNote = new TextNote({
    reviewId: req.body.reviewId,
    note: req.body.note,
  });
  textNote.save()
    .then(() => res.status(201).json({ message: 'Note écrite ajoutée' })).
    catch(error => res.status(400).json({ error: error }));
};
