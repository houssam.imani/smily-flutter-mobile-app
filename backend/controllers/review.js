const Review = require('../models/review.model');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');


exports.getOneReview = (req, res, next) => {
  Review.findOne({
    _id: req.params.id
  }).then(
    (review) => {
      res.status(200).json(review);
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};

exports.getAllReviewByUser = (req, res, next) => {
  Review.find({
    userId: req.params.userId
  }).then(
    (reviews) => {
      res.status(200).json(reviews);
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};

exports.createReview = (req, res, next) => {
  const review = new Review({
    userId: req.body.userId,
    date: req.body.date,
    time: req.body.time,
    mood: req.body.mood,
  });
  review.save()
    .then(() => res.status(201).json({ message: 'Nouveau bilan créé', reviewId: review._id })).
    catch(error => res.status(400).json({ error: error }));
};
