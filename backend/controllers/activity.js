const Activity = require('../models/activity.model');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');


exports.getAllActivitiesByReview = (req, res, next) => {
  Activity.find({
    reviewId: req.params.reviewId
  }).then(
    (activities) => {
      res.status(200).json(activities);
    }
  ).catch(
    (error) => {
      res.status(400).json({
        error: error
      });
    }
  );
};

exports.addActivity = (req, res, next) => {
  const activity = new Activity({
    reviewId: req.body.reviewId,
    activity: req.body.activity,
  });
  activity.save()
    .then(() => res.status(201).json({ message: 'Activity ajouté' })).
    catch(error => res.status(400).json({ error: error }));
};
